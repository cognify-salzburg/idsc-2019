Yellow Stone National Park
Proposing models for Yellow Stone National Park Visits

Context: 
National Park service, as one of the most well-applicated methods to Protected Areas, has been well-known in the US for decade. Until 2005, there are over 100000 protected areas worldwide, covering over 12% of the Earth’s land surface (Chape, Harrison, Spalding, & Lysenko, 2005).

Specifically, the focus is on the human activities happened in Yellowstone, especially the number of visits to this national park. The purpose of possible analysis of this dataset is to propose and inspect the factors that may influence people’s visits to Yellowstone over years, such as weather and climate, travel costs, other macroeconomic factors, and so on.

Source: 
https://www.kaggle.com/snehal1405/yellow-stone-national-park
