Historical Hourly Weather Data 2012-2017
Hourly weather data for 30 US & Canadian Cities + 6 Israeli Cities

Context:
Who amongst us doesn't small talk about the weather every once in a while? 
The goal of this dataset is to elevate this small talk to medium talk.

Just kidding, I actually originally decided to collect this dataset in order to demonstrate basic signal processing concepts, such as filtering, Fourier transform, auto-correlation, cross-correlation, etc..., (for a data analysis course I'm currently preparing). 
I wanted to demonstrate these concepts on signals that we all have intimate familiarity with and hope that this way these concepts will be better understood than with just made up signals.

The weather is excellent for demonstrating these kinds of concepts as it contains periodic temporal structure with two very different periods (daily and yearly).

Content:
The dataset contains ~5 years of high temporal resolution (hourly measurements) data of various weather attributes, such as temperature, humidity, air pressure, etc. 
This data is available for 30 US and Canadian Cities, as well as 6 Israeli cities. 
I've organized the data according to a common time axis for easy use. 
Each attribute has it's own file and is organized such that the rows are the time axis (it's the same time axis for all files), and the columns are the different cities (it's the same city ordering for all files as well). 
Additionally, for each city we also have the country, latitude and longitude information in a separate file.

Acknowledgements:
The dataset was aquired using Weather API on the OpenWeatherMap website, and is available under the ODbL License.

Source:
https://www.kaggle.com/selfishgene/historical-hourly-weather-data
